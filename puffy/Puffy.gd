extends KinematicBody

export var speed = 0.5

var direction = Vector3(1, 0, 0)
var forward_velocity = 0.0

var roll_velocity = 0.0
var direction_velocity = Vector2(0.0, 0.0)

var is_mouse_moving = false

enum AnimationState {
	Still,
	Start,
	Swimming
}

var animation_state = AnimationState.Still

var animation_state_data = {
	Still: {
		"blend_to_start": false,
		"blend_elapsed_time": 0.0,
	},
	Start: {
		"time_since_start": 0.0,
	},
	Swimming: {
		"blend_in": false,
		"blend_in_forward_vel": 0.0,
	},
}

func _ready():
	$AnimationTreePlayer.blend2_node_set_amount("IdleSwimBlend", 0.0)

func _process(delta):
	
	var go_forward = Input.is_key_pressed(KEY_W)
	var roll_left = Input.is_key_pressed(KEY_A)
	var roll_right = Input.is_key_pressed(KEY_D)
	
	#
	# rolling
	#
	
	if roll_left:
		# left/right movement
		roll_velocity += 1.0 * delta
		roll_velocity = min(roll_velocity, 1.0)
	
	if roll_right:
		# left/right movement
		roll_velocity += -1.0 * delta
		roll_velocity = max(roll_velocity, -1.0)
	
	if not roll_left and not roll_right:
		# damp
		roll_velocity = approach_zero(roll_velocity, 2.0 * delta)
	
	#
	# direction
	#
	
	if not is_mouse_moving:
		# no mouse motion last frame, so daming starts
		direction_velocity.x = approach_zero(direction_velocity.x, 4.0 * delta)
		direction_velocity.y = approach_zero(direction_velocity.y, 4.0 * delta)
		
	else:
		# mouse was moving, so flag for daming next frame unless we move again
		is_mouse_moving = false
	
	var forward = -global_transform.basis.z
	var up = -global_transform.basis.y
	var side = global_transform.basis.x
	
	# roll
	rotate(forward, roll_velocity * 0.05)
	
	# look sides
	rotate(up, direction_velocity.x * 0.05)
	rotate(side, direction_velocity.y * 0.05)
	
	#
	# go forward
	#
	
	if go_forward:
		forward_velocity += 1.0 * delta
		forward_velocity = min(forward_velocity, 1.0)
	else:
		# damp speed
		forward_velocity = approach_zero(forward_velocity, 1.0 * delta)
		# forward_velocity -= 1.0 * delta
		# forward_velocity = max(forward_velocity, 0.0)
	
	
	# now adjust the animations
	process_animation(delta)
	
func _physics_process(delta):
	
	orthonormalize()
	
	

	move_and_collide((global_transform.basis.z).normalized() * forward_velocity * speed * delta)

func _input(event):
	
	if event is InputEventMouseMotion:
		var mouse_speed = event.relative
		
		direction_velocity.x += deg2rad(mouse_speed.x) / 2
		direction_velocity.y += deg2rad(mouse_speed.y) / 2
		
		direction_velocity.x = clamp(direction_velocity.x, -1.0, 1.0)
		direction_velocity.y = clamp(direction_velocity.y, -1.0, 1.0)
		
		is_mouse_moving = true


var roll_right_roll_back = false
var roll_left_roll_back = false

func process_animation(delta):
	
	#
	# ROLL
	#
	
	var roll_right_seek = clamp(roll_velocity, -1.0, 0.0) * -1
	
	if roll_right_seek >= 0.99:
		roll_right_roll_back = true
	if roll_right_seek <= 0.01:
		roll_right_roll_back = false
	
	if roll_right_roll_back:
		roll_right_seek = 2.0 - roll_right_seek
	
	$AnimationTreePlayer.timeseek_node_seek("RollRightSeek", roll_right_seek)
	
	var roll_left_seek = clamp(roll_velocity, 0.0, 1.0)
	
	if roll_left_seek >= 0.99:
		roll_left_roll_back = true
	if roll_left_seek <= 0.01:
		roll_left_roll_back = false
	
	if roll_left_roll_back:
		roll_left_seek = 2.0 - roll_left_seek
	
	$AnimationTreePlayer.timeseek_node_seek("RollLeftSeek", roll_left_seek)
	
	#
	# look side
	#
	var look_side = direction_velocity.x + 1.0
	$AnimationTreePlayer.timeseek_node_seek("LookSideSeek", look_side)
	
	#
	# Look vertical
	#
	var look_vertical = -direction_velocity.y + 1.0
	$AnimationTreePlayer.timeseek_node_seek("LookVerticalSeek", look_vertical)
	
	#
	# States
	#
	
	match animation_state:
		AnimationState.Still:
			
			var data = animation_state_data[AnimationState.Still]
			
			if Input.is_key_pressed(KEY_W) and not data.blend_to_start:
				data.blend_to_start = true
				data.blend_elapsed_time = 0.0
				$AnimationTreePlayer.mix_node_set_amount("SwimSpeed", 0.0)
			
			if data.blend_to_start:
				
				# IdleSwimBlend needs to go from 0 to 1
				
				var BLEND_TIME = 0.3
				
				var blend_factor = data.blend_elapsed_time / BLEND_TIME
				
				if blend_factor >= 1.0:
					animation_state = AnimationState.Start
					$AnimationTreePlayer.oneshot_node_start("SwimmingStart")
					var swim_start_data = animation_state_data[AnimationState.Start]
					swim_start_data.time_since_start = 0.0
					data.blend_to_start = false
					return
				
				$AnimationTreePlayer.blend2_node_set_amount("IdleSwimBlend", blend_factor)
				
				data.blend_elapsed_time += delta
		
		AnimationState.Start:
			var data = animation_state_data[AnimationState.Start]
			data.time_since_start += delta
			
			var BLEND_TIME_START = 0.9
			var START_ANIMATION_LENGTH = $AnimationPlayer.get_animation("SwimStart").length
			
			if data.time_since_start >= BLEND_TIME_START:
				# start blending the animation to swim
				var time_since_blend_start = data.time_since_start - BLEND_TIME_START
				var blend_percentage = time_since_blend_start / (START_ANIMATION_LENGTH - BLEND_TIME_START)
				$AnimationTreePlayer.blend2_node_set_amount("IdleSwimBlend", 1.0)
				$AnimationTreePlayer.mix_node_set_amount("SwimSpeed", blend_percentage)
			
			if data.time_since_start >= START_ANIMATION_LENGTH:
				animation_state = AnimationState.Swimming
				var swim_state = animation_state_data[AnimationState.Swimming]
				swim_state.blend_in = true
				swim_state.blend_in_forward_vel = 1.0
		
		AnimationState.Swimming:
			
			var data = animation_state_data[AnimationState.Swimming]
			
			if data.blend_in:
				data.blend_in_forward_vel -= delta
				if data.blend_in_forward_vel <= forward_velocity:
					data.blend_in = false
				else:
					# $AnimationTreePlayer.mix_node_set_amount("SwimSpeed", data.blend_in_forward_vel)
					$AnimationTreePlayer.blend2_node_set_amount("IdleSwimBlend", data.blend_in_forward_vel)
			else:
				# $AnimationTreePlayer.mix_node_set_amount("SwimSpeed", forward_velocity)
				$AnimationTreePlayer.blend2_node_set_amount("IdleSwimBlend", forward_velocity)
				
				if forward_velocity < 0.01:
					animation_state = AnimationState.Still
					$AnimationTreePlayer.blend2_node_set_amount("IdleSwimBlend", 0.0)





#
# UTIL
#
func approach_zero(value, change):
	var side = 0 if value == 0 else (-1 if value > 0.0 else 1)
	value += side * change
	
	if side == -1:
		value = max(value, 0.0)
	elif side == 1:
		value = min(value, 0.0)
	
	return value