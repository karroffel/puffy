extends KinematicBody

signal despawned()

export var direction = Vector3(0, 0, 0)
export var speed = 1.0 # meters/second

export var time_to_live = 30.0

var lifetime = 0.0

func _ready():
	pass

func _physics_process(delta):
	move_and_collide(direction * speed * delta)
	
	lifetime += delta
	
	if lifetime >= time_to_live:
		emit_signal("despawned")
		time_to_live = INF
		# queue_free()