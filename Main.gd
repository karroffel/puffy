extends Spatial

var bubble_scene = preload("res://Bubble.tscn")

onready var puffy = $Puffy
onready var bubbles = $Bubbles

func _ready():
	
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
	randomize()
	
	for i in 10:
		spawn_bubble(
			puffy.global_transform.origin - Vector3(rand_range(-3, 3), rand_range(-3, 3), rand_range(-3, 3)),
			Vector3(rand_range(-1, 1), rand_range(-1, 1), rand_range(-1, 1))
		)
	
	pass
	
func _process(delta):
	
	pass


func spawn_bubble(from, dir):
	var bubble = bubble_scene.instance()
	
	bubble.translation = from
	bubble.direction = dir.normalized()
	bubble.speed = 0.2
	
	bubble.time_to_live = 20 + (10 * randf())
	
	bubbles.add_child(bubble)
	
	bubble.connect("despawned", self, "on_bubble_despawned")

func on_bubble_despawned():
	spawn_bubble(
		puffy.global_transform.origin - Vector3(rand_range(-3, 3), rand_range(-3, 3), rand_range(-3, 3)),
		Vector3(rand_range(-1, 1), rand_range(-1, 1), rand_range(-1, 1))
	)